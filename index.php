<!-- 
    Serving PHP Files:
    php -S localhost:8000
-->
<!-- 
   -"code.php" is used for defining php statements and functions.
   -"index.php"
-->
<?php require_once './code.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>s01 PHP Basics and Selection Control</title>
</head>
<body>
    <!--h1>Hello World!</h1-->
    <!--Single qoute cannot output the variable use-->
    <h1>Echoing Values</h1>
    <p><?php echo'Good day $name! Your given email is $email'; ?></p>
    <!-- Single qoute can be use but concatenation is needed (.) is concatenate operator -->
    <p><?php echo'Good day ' .$name. '!' .'Your given email is '.$email.'.'; ?></p>
    <!-- Double qoute can easily read the variable-->
    <p><?php echo"Good day  $name Your given email is $email"; ?></p>
    <p><?php echo 'The value of pi is: ' .PI; ?></p>

    <h1>Data types</h1>
    <p><?php echo $hasTravelledABroad; ?></p>
    <p><?php echo $spouse; ?></p>

    <!-- To see thier types instead, we can use var_dump() function  -->
    <p><?php echo gettype($hasTravelledABroad); ?></p>
    <p><?php echo gettype($spouse); ?></p>

    <p><?php echo var_dump($hasTravelledABroad); ?></p>
    <p><?php echo var_dump($spouse); ?></p>

    <p><?php echo var_dump($gradesObj); ?></p>
    <p><?php echo $gradesObj->firstGrading; ?></p>
    <p><?php echo $personObj->address->state; ?></p>

    <p><?php echo $grades[3]; ?></p>
    <p><?php echo $grades[2]; ?></p>

    <h1>Operators</h1>
    <p>X: <?php echo $x; ?></p>
    <p>Y: <?php echo $y; ?></p>

    <p>is Legal Age: <?php var_dump($isLegalAge); ?></p>
    <p>is Legal Age: <?php var_dump($isRegistered); ?></p>

    <h2>Arithmetic Operators</h2>

    <p>Sum: <?php echo $x + $y; ?></p>
    <p>Difference: <?php echo $x - $y; ?></p>
    <p>Product: <?php echo $x * $y; ?></p>
    <p>Qoutient: <?php echo $x / $y; ?></p>
    <p>Module: <?php echo $x % $y; ?></p>

    <h2>Equality Operator</h2>

    <p>Loose Equality: <?php var_dump($x == '1342.14') ?></p>
    <p>Strict Equality: <?php var_dump($x === '1342.14') ?></p>
    <p>Loose Inequality: <?php var_dump($x != '1342.13')?></p>

    <h2>Greater/Lesser Operator</h2>
    <p>Is Lesser: <?php var_dump($x < $y) ?></p>
    <p>Is Greater: <?php var_dump($x > $y) ?></p>
    <p>Is Lesser or Equal: <?php var_dump($x <= $y) ?></p>
    <p>Is Greater or Equal: <?php var_dump($x >= $y) ?></p>

    <h2>Logical Operators</h2>
    <p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
    <p>Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
    <p>Are All Requirements Not Met: <?php var_dump($isLegalAge && !$isRegistered); ?></p>

    <p>Full Name: <?php echo getFullName('John', 'D', 'Smith'); ?></p>
    
    <p>Ternary</p>
    <p><?php echo determineTyphoonIntensity(12); ?></p>
    <p><?php echo isUnderAge(12); ?></p>
    <p><?php echo isUnderAge(19); ?></p>

    <p>Switch</p>
    <p><?php echo fave('red'); ?></p>



</html>
